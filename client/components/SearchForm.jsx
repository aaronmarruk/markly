import React, { Component, PropTypes } from 'react';

export default class SearchForm extends Component {

  render() {
    let cls = `fl ba bw1 b--light-gray br2 f6 f5-l input-reset black-80 bg-white
     fl pa2 lh-solid w-100 mr5`;

    return (
      <div className="ph4 pv3">
        <div className="cf bn ma0 pa0">
          <div className="cf">
            <input placeholder="Search notes..." className={cls} type="text" />
          </div>
        </div>
      </div>
    );
  }
}
