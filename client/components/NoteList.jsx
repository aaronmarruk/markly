import React, { Component, PropTypes } from 'react';
import NoteListItem from './NoteListItem.jsx';

export default class NoteList extends Component {

  renderNotes() {
    return this.props.notes.map((note) => {
      return (
        <NoteListItem
          key={note._id}
          note={note}
          selectNote={this.props.selectNote}
          selectedNote={this.props.selectedNote}
          updateTitle={this.props.updateNoteTitle} />
      );
    });
  }

  render() {
    return (
      <div className="notelist overflow-scroll">
        <ul className="list pl0 ma0 bt b--black-10">
          {this.renderNotes()}
        </ul>
      </div>
    );
  }
}

NoteList.propTypes = {
  notes: PropTypes.object.isRequired,
  selectedNote: PropTypes.object.isRequired,
  selectNote: PropTypes.func.isRequired,
  updateNoteTitle: PropTypes.func.isRequired
};
