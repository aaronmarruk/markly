import React from 'react';
import App from './components/App.jsx';
import Editor from './views/Editor.jsx';
import Login from './views/Login.jsx';
import Register from './views/Register.jsx';
import Homepage from './views/Homepage.jsx';

function redirectIfNotSignedIn(context, redirect) {
  AppLibRedirectPath = context.path;

};

FlowRouter.route('/', {
  name: 'home',
  triggersEnter: [redirectIfNotSignedIn],
  action: function() {
    var notSignedIn = !Meteor.userId();
    if (notSignedIn) {
      ReactLayout.render(App, {
        content: <Homepage />
      });
    } else {
      console.log("This in router", this);
      ReactLayout.render(App, {

        content: <Editor />
      });
    }
  }
});

FlowRouter.route('/login', {
  name: 'login',
  action: function() {
    ReactLayout.render(App, {

      content: <Login />
    });
  }
});

FlowRouter.route('/register', {
  name: 'register',
  action: function() {
    ReactLayout.render(App, {
      content: <Register />
    });
  }
});

FlowRouter.notFound = {
  action: function() {
   FlowRouter.go('home');
  }
};
