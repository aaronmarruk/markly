import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('notes', function notesPublication() {
    let userId = this.userId;
    return Notes.find({ userId }, {sort: {updatedAt: -1}});
  });
}

Meteor.methods({
  'notes.insert'(title, content) {
    check(title, String);
    check(content, String);

    // Make sure the user is logged in before inserting a note
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    let attrs = {
      title,
      content,
      createdAt: new Date(),
      updatedAt: new Date(),
      userId: this.userId
    };

    Notes.insert(attrs);
  },

  'notes.remove'(noteId) {
    check(noteId, String);

    Notes.remove(noteId);
  },

  'notes.updateTitle'(noteId, title) {
    check(noteId, String);
    check(title, String);

    // Make sure the user is logged in before updating a note
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Notes.update(noteId, { $set: { title, updatedAt: new Date() } });
  },

  'notes.updateContent'(noteId, content) {
    check(noteId, String);
    check(content, String);

    // Make sure the user is logged in before updating a note
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Notes.update(noteId, { $set: { content, updatedAt: new Date() } });
  }
});
