import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import NoteList from '../components/NoteList.jsx';
import SearchForm from '../components/SearchForm.jsx';
import Button from 'react-button';
import SplitPane from 'react-split-pane';
import Editor from 'react-md-editor';

class EditorView extends Component {
  constructor() {
    super(...arguments);
    Meteor.subscribe('notes');

    this.state = {
      selectedNote: null,
      markdown: ''
    };
  }

  newNote() {
    let
      title = 'Untitled note',
      content = 'Write something nice'
    ;

    Meteor.call('notes.insert', title, content, function(err, result) {
      if (err) { console.log('there was an error: ' + err.reason); };
    });
  };

  selectNote = (note, e) => {
    e.preventDefault();

    this.setState({
      selectedNote: note,
      markdown: note.content
    });
  };

  editorChanged = (markdown) => {
    this.setState({markdown});

    Meteor.call('notes.updateContent',
      this.state.selectedNote._id, markdown,
      function(err, result) {
        if (err) { console.log('there was an error: ' + err.reason); };
      }
    );
  }

  updateNoteTitle(value) {
    Meteor.call('notes.updateTitle',
      this.props.note._id, value.text,
      function(err, result) {
        if (err) { console.log('there was an error: ' + err.reason); };
      }
    );
  };

  renderNoteList() {
    return this.props.subsReady?
      <NoteList
        notes={this.props.notes}
        selectNote={this.selectNote}
        selectedNote={this.state.selectedNote}
        updateNoteTitle={this.updateNoteTitle} />
    :
      <div className="loading">
        Loading...
      </div>
    ;
  };

  renderEditor() {
    let val = this.state.markdown;

    let opts = {
      "lineWrapping": true,
      "viewportMargin": Infinity
    };

    return (
      <Editor
        value={val}
        options={opts}
        onChange={this.editorChanged}
      />
    );
  }

  render() {
    return (
      <SplitPane split="vertical" defaultSize={160} >
        <div className="bg-dark-gray h-100">
          <ul className="moon-gray list ma0 pt4">
            <li className="b mb3">Marky</li>
            <li onClick={this.newNote.bind(this)} className="mb3">New +</li>
            <li className="mb3">Settings</li>
            <li className="mb3 absolute bottom-1">Aaron</li>
          </ul>
        </div>
        <SplitPane split="vertical" defaultSize={350}>
          <div>
            <SearchForm />
            <div className="bg-near-white">
              {this.renderNoteList()}
            </div>
          </div>
          <div className="">
            <div className="center mw6">
              {this.renderEditor()}
            </div>
          </div>
        </SplitPane>

      </SplitPane>

    );
  }
}

export default createContainer(() => {

  const
    subscription = Meteor.subscribe("notes"),
    subsReady = subscription.ready()
  ;

  return {
    subsReady: subsReady,
    notes: Notes.find({}, {sort: { updatedAt: -1 }}).fetch()
  }

}, EditorView);
