import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
//import Notes from '../../server/publications/notes.js';

class App extends Component {
  render() {

    return this.props.subReady?
      <div className="app-container avenir">
       <main className="container">
         {this.props.content}
       </main>
     </div>
    :
      <div className="app-container">
        Loading...
      </div>
    ;

  }
};

App.propTypes = {
  //notes: PropTypes.array.isRequired,
};

export default createContainer(() => {


  let currentUser;

  const
    subscription = Meteor.subscribe("userData"),
    subReady = subscription.ready();

  if (subReady) {
    currentUser = Meteor.user();
  };

  return {
    subReady: subReady,
    currentUser: currentUser,
    signedIn: Meteor.user() != null,
    //notes: Notes.find({}, { sort: { updatedAt: -1 } }).fetch(),
  };
}, App);
// import { createContainer } from 'meteor/react-meteor-data';
// import { Notes } from '../api/notes.js';
// import NoteListItem from './NoteListItem.jsx';
// import Button from 'react-button';
// import SplitPane from 'react-split-pane';
// Editor = require('react-md-editor');

// // App component - represents the whole app
// class App extends Component {
//   constructor() {
//     super(...arguments);
//     this.state = {
//       selectedNote: null,
//       markdown: ''
//     };
//   }

//   selectNote = (note, e) => {
//     e.preventDefault();
//     console.log(note, e);
//     this.setState({selectedNote: note});
//     this.setState({markdown: note.content});

//     Notes.update(noteId, { $set: { updatedAt: new Date()}, });
//   };

//   newNote = () => {

//     let title = 'Untitled note';
//     let content = 'Write something nice';

//     Notes.insert({
//       title,
//       content,
//       createdAt: new Date(), // current time
//       updatedAt: new Date()
//     });

//   };

//   updateNoteTitle(value) {
//     var noteId = this.props.note._id;
//     Notes.update(noteId, { $set: { title: value.text, updatedAt: new Date()}, });
//   };

//   renderNoteList() {
//     return this.props.notes.map((note) => (
//       <NoteListItem
//         key={note._id}
//         note={note}
//         selectNote={this.selectNote}
//         selectedNote={this.state.selectedNote}
//         updateTitle={this.updateNoteTitle} />
//     ));
//   };

//   editorChanged = (value) => {
//     this.setState({markdown: value});
//     Notes.update(this.state.selectedNote._id, { $set: { content: value, updatedAt: new Date()}, });
//     //this.setState({selectedNote: note});
//   }

//   renderNoteControls() {
//     return (

//         <div className="ph4 pv3">
//           <div className="cf bn ma0 pa0">
//             <div className="cf">
//               <input placeholder="Search notes..." className="fl ba bw1 b--light-gray br2 f6 f5-l input-reset black-80 bg-white fl pa2 lh-solid w-100 mr5" type="text" />
//             </div>
//           </div>
//         </div>


//     );
//   }

//   renderEditor() {


//       let val = this.state.markdown;

//       let opts = {
//         "lineWrapping": true,
//         "viewportMargin": Infinity
//       };
//       return (
//         <Editor
//           value={val}
//           options={opts}
//           onChange={this.editorChanged}
//         />);


//   }


//   render() {
//     return (


//       <SplitPane split="vertical" defaultSize={160} >
//         <div className="bg-dark-gray h-100">
//           <ul className="moon-gray list ma0 pt4">
//             <li className="b mb3">Marky</li>
//             <li onClick={this.newNote} className="mb3">New +</li>
//             <li className="mb3">Settings</li>
//             <li className="mb3 absolute bottom-1">Aaron</li>
//           </ul>
//         </div>
//         <SplitPane split="vertical" defaultSize={350}>

//           <div className="bg-near-white">
//             {this.renderNoteControls()}
//             <div className="notelist overflow-scroll">
//               <ul className="list pl0 ma0 bt b--black-10">
//                 {this.renderNoteList()}
//               </ul>
//             </div>
//           </div>
//           <div className="">
//             <div className="center mw6">
//               {this.renderEditor()}
//             </div>
//           </div>
//         </SplitPane>

//       </SplitPane>

//     );
//   }
// }

// App.propTypes = {
//   notes: PropTypes.array.isRequired,
// };

// export default createContainer(() => {
//   return {
//     notes: Notes.find({}).fetch(),
//   };
// }, App);
