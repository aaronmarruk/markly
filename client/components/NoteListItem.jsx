import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { RIEInput } from 'riek';

// NoteListItem, a single item in the list of notes in UI. Each item can trigger
// "select" events. RIEInput also fires events to do with inline editing of
// title fields.
export default class NoteListItem extends Component {
  render() {

    let
      // is true if the current note in the list is "selected"
      selected = this.props.selectedNote && this.props.note._id ==
        this.props.selectedNote._id,

      // is true if the current note in the list is "unselected"
      unselected = !this.props.selectedNote || this.props.selectedNote &&
        this.props.note._id != this.props.selectedNote._id,

      // set up some classlists, depending on whether the item is selected or no
      liClasses = classNames({
        'fw5 bb b--black-10': true,
        'bg-white': selected,
        'bg-near-white': unselected,
      }),

      divClasses = classNames({
        'pv3 ph4 bl bw2': true,
        'b--silver': selected,
        'b--white': unselected,
      }),

      titleClasses = "dib f5 mb1",
      contentClasses = "f6 fw4 db light-silver normal lh-copy measure",

      length = 120,
      trimmedContent = this.props.note.content.substring(0, length)
    ;

    return (
      <li
        key={this.props.note._id}
        onClick={(e)=> {this.props.selectNote(this.props.note, e)}}
        className={liClasses} >
        <div className={divClasses}>
          <RIEInput
            value={this.props.note.title}
            change={this.props.updateTitle.bind(this)}
            propName="text"
            className={titleClasses}
            validate={this.isStringAcceptable}
            classLoading="loading"
            classInvalid="invalid" />
          <span className={contentClasses}>{trimmedContent}</span>
        </div>
      </li>
    );
  }
}

NoteListItem.propTypes = {
  key: PropTypes.number.isRequired,
  note: PropTypes.object.isRequired,
  selectedNote: PropTypes.object.isRequired,
  selectNote: PropTypes.func.isRequired,
  updateTitle: PropTypes.func.isRequired
};

